<!-- Should be the same format as in CHANGELOG.md -->
\<version\> (\<yyyy-mm-dd\>)

<!-- You can copy the changelog from CHANGELOG.md -->
## Changelog
