## Merge request checklist

This MR fulfills the following requirements:

- [ ] Docs have been reviewed and added / updated
- [ ] `make test` is not producing errors
- [ ] Commit messages adhere to the given commitizen format

## Merge request type

Type of change this MR introduces:

- [ ] Bugfix
- [ ] Feature
- [ ] Code style update (formatting, renaming)
- [ ] Refactoring (no functional changes, no api changes)
- [ ] Build related changes
- [ ] Documentation content changes

## Does this introduce a breaking change?

- [ ] Yes  
- [ ] No

## Description

A clear and concise description.

## Related MRs/Issues

Link to related PRs/Issues

## Related User Stories

Link to related US from Jira board

## To Reproduce

Steps to reproduce the behavior:
(eg: Go to '...'; Click on '....'; See error; )

### Expected behavior

A clear and concise description of what you expected to happen.

### Screenshots

If applicable, add screenshots to help explain your problem.  
⬅️  _before_  
➡️  _after_  
